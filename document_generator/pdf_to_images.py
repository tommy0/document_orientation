from pdf2image import convert_from_path
import os
import settings


def create_dir(path: str):
    if not os.path.isdir(path):
        os.mkdir(path)


def process_document(doc_path: str, out_path: str, dpi: int = 300):
    try:
        pages = convert_from_path(doc_path, dpi)
        for i, page in enumerate(pages):
            page_path = os.path.join(out_path, f'{i}.png')
            page.save(page_path, 'PNG')
    except Exception as e:
        print(f"Error convert {doc_path}, because: {e}")


def process_dataset(ds_path: str):
    ds_name = os.path.basename(ds_path)
    result_base_path = os.path.join(settings.IMAGE_DIR, ds_name)
    create_dir(result_base_path)
    for doc in os.listdir(ds_path):
        print(f"process {doc}")
        images_out_path = os.path.join(result_base_path, doc.split(".")[0])
        create_dir(images_out_path)
        process_document(os.path.join(ds_path, doc), images_out_path)
        print(f"done {doc}")


if __name__ == '__main__':
    # "google"
    # "oldmemory"
    # "new_newspaper
    # "id19"
    process_dataset(os.path.join(settings.PDF_DIR, "gult_times"))
