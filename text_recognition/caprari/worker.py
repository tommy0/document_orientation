import cv2
import numpy as np
import os
import settings

base_directory = os.path.dirname(os.path.abspath(__file__))


def read_gray_image(path: str) -> np.ndarray:
    img = cv2.imread(os.path.join(directory, path), cv2.IMREAD_GRAYSCALE)
    return img


def invert_gray_image(img: np.ndarray):
    return 255 - img


def show_gist():
    pass


if __name__ == '__main__':
    # img = cv2.imread('tuto5.png', 1)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    directory = os.path.join(settings.IMAGE_DIR, "new_newspaper")
    files = os.listdir(directory)

    up = 0
    total = 0
    thresh = 0.4
    for j, f in enumerate(files):
        gg = os.path.join(directory, f)
        imgg = os.listdir(gg)
        for k, im in enumerate(imgg):
            pa = os.path.join(gg, im)
            print(pa)
            img = read_gray_image(pa)
            #gg = cv2.rotate(img, cv2.ROTATE_180)
            #cv2.imwrite('tuto_rott.png', gg)
            if img is None:
                print(f"errror {f} {im}")
                continue
            total += 1
            #Invert
            img = 255 - img

            # Calculate horizontal projection
            proj = np.sum(img, 1)

            # Create output image same height as text, 500 px wide
            m = np.max(proj)
            w = 500
            h = proj.shape[0]
            result = np.zeros((proj.shape[0], 500))

            first = 100

            last = ((h - first) // 10) * 10
            ranger = int((last - first) / 10)


            # # Draw a line for each row
            # for row in range(img.shape[0]):
            #     cv2.line(result, (0, row), (int(proj[row] * w / m), row), (255, 255, 255), 1)

            # Save result
            # cv2.imshow("lol", result)
            # cv2.waitKey(0)
            csdtp = np.zeros((ranger-1, ), np.float64)
            cssdtp = np.zeros((ranger - 1,), np.float64)
            for base in range(first, last, ranger):

                imgg = img[base: base+ranger, :]
                p = proj[base: base+ranger]

                tr = thresh*np.max(p)
                tp = np.ndarray(p.shape)

                for i, value in np.ndenumerate(p):
                    tp[i] = min(p[i], tr)

                dtp = tp[1:ranger] - tp[:(ranger - 1)]
                sdtp = np.power(dtp, 2)

                ssdtp = np.ndarray(sdtp.shape)

                for i, value in np.ndenumerate(sdtp):
                    ssdtp[i] = sdtp[i] if dtp[i] >= 0 else -sdtp[i]

                csdtp = csdtp + sdtp
                cssdtp = cssdtp + ssdtp

            asym = - np.sum(cssdtp) / np.sum(csdtp)
            if asym <= 0:
                print(f"{k}: UP")
                up += 1
            else:
                print(f"{j} DOWN")
        print(up)
        print(total)
