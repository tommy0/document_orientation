# from pdf2image import convert_from_bytes
# from PyPDF2 import PdfFileWriter, PdfFileReader
# import settings
# import io
# import sys
# import os
#
# # if len(sys.argv)>1:
# #     input_file = sys.argv[1]
# # else:
# #     input_file = input("\x1b[0;33mType the PDF file name to create cover photo:\x1b[0m ")
#
# inp = PdfFileReader(os.path.join(settings.PDF_DIR, '1.pdf'), "rb")
# page = inp.getPage(0)
#
# wrt = PdfFileWriter()
# wrt.addPage(page)
#
# r = io.BytesIO()
# wrt.write(r)
#
# images = convert_from_bytes(r.getvalue())
# images[0].save("1.png")
# print("\x1b[0;33mDone, your cover photo has been saved as {}.\x1b[0m".format("1.png"))
# r.close()


from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from wand.image import Image
from pdf2image import convert_from_path
import uuid

# output = PdfFileWriter()
# input1 = PdfFileReader(open("1.pdf", "rb"))

# print how many pages input1 has:
# print("document1.pdf has %d pages." % input1.getNumPages())

# add page 1 from input1 to output document, unchanged
# output.addPage(input1.getPage(0))
#
# src_pdf = PdfFileReader(open("1.pdf", "rb"))
#
# dst_pdf = PdfFileWriter()
# dst_pdf.addPage(src_pdf.getPage(0))
#
# pdf_bytes = io.BytesIO()
# dst_pdf.write(pdf_bytes)
# pdf_bytes.seek(0)
#
# images = convert_from_bytes(pdf_bytes.getvalue(),dpi=200,fmt='png')
# img = Image(file = pdf_bytes, resolution = 200)
# img.convert("png")
images = convert_from_path(f'{i}.pdf')
print(len(images))
for image in images:
    image.save(f"{uuid.uuid4()}.png")
# img.save(filename = '1.png')


# Main
# ====

