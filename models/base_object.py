class BaseObject(object):
    def __init__(self):
        pass

    def get_all_objects(self):
        raise NotImplemented

    def get_random_object(self):
        raise NotImplemented

    def get_random_objects(self, count: str):
        raise NotImplemented
