__all__ = [
    'ServiceResult',
    'Service',
    'guard_service_call'
]


def guard_service_call(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            return ServiceResult(error_reason=str(e))
    return func_wrapper


class ServiceResult:
    result: object = None
    error_reason: str = None

    def __init__(self, result: object = None, error_reason: str = None):
        self.result = result
        self.error_reason = error_reason

    @property
    def is_successful(self) -> bool:
        return self.error_reason is None


class Service:
    def call(self) -> ServiceResult:
        raise NotImplementedError
