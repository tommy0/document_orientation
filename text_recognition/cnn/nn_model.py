# Create your first MLP in Keras
from keras.models import Sequential
from keras import layers
import numpy
import os
import settings
import cv2

# fix random seed for reproducibility
numpy.random.seed(7)
# load pima indians dataset
#dataset = numpy.loadtxt("meta.csv", delimiter=",")
#split into input (X) and output (Y) variables
# X = dataset[:,1]
# Y = dataset[:,1]
# create model
# Test model 1
img_input = layers.Input(shape=(224, 224, 1))
x = layers.Conv2D(64, (3, 3),
                      activation='relu',
                      padding='same',
                      name='block1_conv1')(img_input)
x = layers.Conv2D(64, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block1_conv2')(x)
x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x)

# Block 2
x = layers.Conv2D(128, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block2_conv1')(x)
x = layers.Conv2D(128, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block2_conv2')(x)
x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x)

# Block 3
x = layers.Conv2D(256, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block3_conv1')(x)
x = layers.Conv2D(256, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block3_conv2')(x)
x = layers.Conv2D(256, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block3_conv3')(x)
x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x)

# Block 4
x = layers.Conv2D(512, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block4_conv1')(x)
x = layers.Conv2D(512, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block4_conv2')(x)
x = layers.Conv2D(512, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block4_conv3')(x)
x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x)

x = layers.Conv2D(512, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block5_conv1')(x)
x = layers.Conv2D(512, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block5_conv2')(x)
x = layers.Conv2D(512, (3, 3),
                  activation='relu',
                  padding='same',
                  name='block5_conv3')(x)
x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x)
x = layers.Dense(4064, activation='relu', name="dense_1")(x)
x = layers.Dense(4064, activation='relu', name="dense_2")(x)
x = layers.Softmax(2, name="softmax_2")(x)
#Fit the model
#x.fit(X, Y, epochs=400, batch_size=80)
#evaluate the model

directory = os.path.join(settings.IMAGE_DIR, "new_newspaper")
files = os.listdir(directory)

for j, f in enumerate(files):
    gg = os.path.join(directory, f)
    imgg = os.listdir(gg)
    for k, im in enumerate(imgg):
        pa = os.path.join(gg, im)
        img = cv2.imread(pa, 1)
        img = cv2.resize(img, (224, 224))
        scores = x.evaluate(img)
        print("\n%s: %.2f%%" % (x.metrics_names[1], scores[1]*100))
