from utils.service_base import Service, ServiceResult


class ImageGeneratorServiceResult(ServiceResult):
    pass


class ImageGeneratorService(Service):
    def __init__(self, seed: int):
        self.seed = seed

    def call(self) -> ImageGeneratorServiceResult:
        #call model about text generating
        #call generate pdf with text and seed of fonts and ...
        #call pdf to png
        pass
