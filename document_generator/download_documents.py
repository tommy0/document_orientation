import os

import settings

from urllib.request import urlopen, urlretrieve

from itertools import chain
from html.parser import HTMLParser


class LinkParser(HTMLParser):
    def reset(self):
        HTMLParser.reset(self)
        self.links = iter([])

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for name, value in attrs:
                if name == 'href':
                    if value.endswith(".pdf"):
                        self.links = chain(self.links, [value])

def gen_links(f, parser):
    encoding = f.headers.get_content_charset() or 'UTF-8'

    for line in f:
        parser.feed(line.decode(encoding))
        yield from parser.links


def create_dir(path: str):
    if not os.path.isdir(path):
        os.mkdir(path)


def download_pdf_from_new_newspaper(base_url: str, max_number: int):
    dir_name = os.path.join(settings.PDF_DIR, 'new_newspaper')
    create_dir(dir_name)
    for i in range(1107, max_number + 1):
        try:
            print(f"START {i}")
            parser = LinkParser()
            f = urlopen(f'{base_url}/{i}/')
            links = gen_links(f, parser)
            ll = list(links)
            print(ll)
            if not ll:
                continue
            link = ll[0]
            file_path = os.path.join(dir_name, f"{i}.pdf")
            print(file_path)
            urlretrieve(link, file_path)
            print(f"DONE {i}")
        except Exception as e:
            print(f"Exception as {e}")
            continue


def dowload_pdf_from_gulf_times(base_url: str):
    dir_name = os.path.join(settings.PDF_DIR, 'gult_times')
    create_dir(dir_name)
    i = 0
    try:
        print(f"START {i}")
        parser = LinkParser()
        f = urlopen(f'{base_url}')
        links = gen_links(f, parser)
        ll = list(links)
        print(ll)
        for link in links:
            file_path = os.path.join(dir_name, f"{i}.pdf")
            print(file_path)
            urlretrieve(link, file_path)
            print(f"DONE {i}")
            i += 1
    except Exception as e:
        print(f"Exception as {e}")


def dowload_pdf_from_oldmemory(base_url: str):
    dir_name = os.path.join(settings.PDF_DIR, 'oldmemory')
    create_dir(dir_name)
    i = 0
    try:
        print(f"START {i}")
        parser = LinkParser()
        f = urlopen(f'{base_url}')
        links = gen_links(f, parser)
        ll = list(links)
        print(ll)
        for link in links:
            file_path = os.path.join(dir_name, f"{i}.pdf")
            print(file_path)
            urlretrieve(link, file_path)
            print(f"DONE {i}")
            i += 1
    except Exception as e:
        print(f"Exception as {e}")


def dowload_pdf_from_google(base_url: str):
    dir_name = os.path.join(settings.PDF_DIR, 'google')
    create_dir(dir_name)
    i = 0
    try:
        print(f"START {i}")
        parser = LinkParser()
        f = urlopen(f'{base_url}')
        links = gen_links(f, parser)
        ll = list(links)
        print(ll)
        for link in links:
            file_path = os.path.join(dir_name, f"{i}.pdf")
            print(file_path)
            urlretrieve(link, file_path)
            print(f"DONE {i}")
            i += 1
    except Exception as e:
        print(f"Exception as {e}")


if __name__ == '__main__':
    # download_pdf_from_new_newspaper("http://novayagazeta.spb.ru/pdfs/", 2078)
    # dowload_pdf_from_gulf_times("https://www.gulf-times.com/PDF")
    # dowload_pdf_from_oldmemory("http://oldmemory.ru/?file=page")
    dowload_pdf_from_google("https://news.google.com/newspapers")
