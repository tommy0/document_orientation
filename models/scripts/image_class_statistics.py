import os
import ujson
import settings
from utils.image_utils import get_meta_name
from utils.decorators import exception_decorator


def __init_statistics_dict() -> dict:
    res = {}
    for key in settings.CLASS_MAP.keys():
        res[key] = 0
    return res


@exception_decorator
def get_statistics() -> dict:
    stat_dict = __init_statistics_dict()
    imgs = os.listdir(settings.DATA_DIR)
    for img in imgs:
        meta_img_name = get_meta_name(name=img)
        meta_path = os.path.join(settings.META_DIR, meta_img_name)

        with open(meta_path, "r") as f:
            data = ujson.load(f)

        stat_dict[data["class"]] += 1

    return stat_dict


@exception_decorator
def print_statistics(res: dict):
    for key, value in res.items():
        print(f"class_number: {key}, name: {settings.CLASS_MAP[key]}, amount: {value}")


if __name__ == '__main__':
    res = get_statistics()
    print_statistics(res)
