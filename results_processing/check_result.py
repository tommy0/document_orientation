import os
from settings import IMAGE_DIR, META_DIR
from utils.logger import app_logger


class ResultChecker:
    def __init__(self):
        self.count = 0
        self.error_count = 0

    def start(self):
        for file in os.listdir(IMAGE_DIR):
            app_logger.info(f"Processing document images in dir: {file}")
            if not os.path.isdir(file):
                continue

            for img in os.listdir(file):
                try:
                    app_logger.info(f"Processing image in dir: {img}")

                    self.count = 0
                except Exception as e:
                    app_logger.error(f"Error processing image in dir: {img}, because: {e}")
                    self.error_count = 0


