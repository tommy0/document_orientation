# -*- coding: utf-8 -*-
from fpdf import FPDF
import random
import string

title = '20000 Leagues Under the Seas'

def generate_text():
    text = []
    for i in range(0, 3000):
        word = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase) for _ in range(random.randint(1, 10)))
        text.append(str(i*99999213))
    return " ".join(text)

class PDF(FPDF):
    def header(self):
        #self.set_compression(0)
        #self.image('img1.jpg', 50, 10, 100)
        # Arial bold 15
        # self.set_font('Arial', 'B', 15)
        # # Calculate width of title and position
        # w = self.get_string_width(title) + 6
        # self.set_x((210 - w) / 2)
        # # Colors of frame, background and text
        # self.set_draw_color(0, 80, 180)
        # self.set_fill_color(230, 230, 0)
        # self.set_text_color(220, 50, 50)
        # # Thickness of frame (1 mm)
        # self.set_line_width(1)
        # # Title
        # self.cell(w, 9, title, 1, 1, 'C', 1)
        # # Line break
        self.ln(10)
        pass

    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Text color in gray
        self.set_text_color(128)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()), 0, 0, 'C')

    def chapter_title(self, num, label):
        # Arial 12
        self.set_font('Arial', '', 12)
        # Background color
        self.set_fill_color(200, 220, 255)
        # Title
        self.cell(0, 6, 'Chapter %d : %s' % (num, label), 0, 1, 'L', 1)
        # Line break
        self.ln(4)

    def chapter_body(self, name):
        #Read text file
        with open(name, 'rb') as fh:
            txt = fh.read().decode('utf-8')
        #txt = generate_text()
        # Times 12
        self.set_font('Arial', '', 18)
        # Output justified text
        self.multi_cell(0, 6, txt)
        # Line break
        self.ln()
        self.set_font('times', '', 12)
        self.multi_cell(0, 6, txt)
        # Mention in italics
        self.set_font('', 'I')
        self.cell(0, 5, '(end of excerpt)')

    def print_chapter(self, num, title, name):
        self.add_page()
        self.chapter_title(num, title)
        self.chapter_body(name)

for i in range(1000):
    pdf = PDF()
    pdf.set_title(title)
    pdf.set_author('Jules Verne')
    pdf.print_chapter(1, 'A RUNAWAY REEF', 'text.txt')
    pdf.output(f'{i}.pdf', 'F')



