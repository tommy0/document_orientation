import logging
from logging.config import dictConfig
from settings import LOG_FILE

LOG_DICT = {
    'version': 1,
    'disable_existing_loggers': True,

    'formatters': {
        'default': {
            'format': '[%(asctime)s][%(levelname)s] %(name)s '
                      '%(filename)s:%(funcName)s:%(lineno)d || %(message)s',
        },
    },

    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': LOG_FILE,
            'mode': 'w',
            'formatter': 'default',
        },
    },

    'loggers': {
        'main': {
            'handlers': ('console', 'file'),
            'level': 'DEBUG',
        },
    }
}


dictConfig(LOG_DICT)
app_logger = logging.getLogger('main')
