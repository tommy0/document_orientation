import os
import ujson
import settings
from utils.image_utils import get_meta_name
from utils.decorators import exception_decorator


@exception_decorator
def create_all_file():
    imgs = os.listdir(settings.DATA_DIR)
    for img in imgs:
        meta_img_name = get_meta_name(name=img)
        meta_path = os.path.join(settings.META_DIR, meta_img_name)

        with open(meta_path, "r") as f:
            data = ujson.load(f)

        with open(settings.ALL_INDEXES_FILE, "w+") as w:
            w.write(f"{data['path']} {data['class']}")


if __name__ == '__main__':
    create_all_file()
