from __future__ import print_function
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.layers.wrappers import TimeDistributed
import argparse
import numpy as np


# method for generating text
def generate_text(model, length, vocab_size, ix_to_char) -> str:
    # starting with random character
    ix = [np.random.randint(vocab_size)]
    y_char = [ix_to_char[ix[-1]]]
    X = np.zeros((1, length, vocab_size))
    for i in range(length):
        # appending the last predicted character to sequence
        X[0, i, :][ix[-1]] = 1
        print(ix_to_char[ix[-1]], end="")
        ix = np.argmax(model.predict(X[:, :i+1, :])[0], 1)
        y_char.append(ix_to_char[ix[-1]])
    return ('').join(y_char)


# method for preparing the training data
def load_data(data_dir, seq_length):
    data = open(data_dir, 'r').read()
    chars = list(set(data))
    VOCAB_SIZE = len(chars)

    print('Data length: {} characters'.format(len(data)))
    print('Vocabulary size: {} characters'.format(VOCAB_SIZE))

    ix_to_char = {ix: char for ix, char in enumerate(chars)}
    char_to_ix = {char: ix for ix, char in enumerate(chars)}

    X = np.zeros((len(data)//seq_length, seq_length, VOCAB_SIZE))
    y = np.zeros((len(data)//seq_length, seq_length, VOCAB_SIZE))
    for i in range(0, len(data)//seq_length):
        X_sequence = data[i*seq_length:(i+1)*seq_length]
        X_sequence_ix = [char_to_ix[value] for value in X_sequence]
        input_sequence = np.zeros((seq_length, VOCAB_SIZE))
        for j in range(seq_length):
            input_sequence[j][X_sequence_ix[j]] = 1.
            X[i] = input_sequence

        y_sequence = data[i*seq_length+1:(i+1)*seq_length+1]
        y_sequence_ix = [char_to_ix[value] for value in y_sequence]
        target_sequence = np.zeros((seq_length, VOCAB_SIZE))
        for j in range(seq_length):
            target_sequence[j][y_sequence_ix[j]] = 1.
            y[i] = target_sequence
    return X, y, VOCAB_SIZE, ix_to_char


def init_args() -> dict:
    ap = argparse.ArgumentParser()
    ap.add_argument('-data_dir', default='./data/test.txt')
    ap.add_argument('-batch_size', type=int, default=50)
    ap.add_argument('-layer_num', type=int, default=3)
    ap.add_argument('-seq_length', type=int, default=50)
    ap.add_argument('-hidden_dim', type=int, default=700)
    ap.add_argument('-generate_length', type=int, default=500)
    ap.add_argument('-nb_epoch', type=int, default=200)
    ap.add_argument('-mode', default='train')
    ap.add_argument('-weights', default='')
    return vars(ap.parse_args())


class Data:
    def __init__(self, args):
        self.data_dir = args['data_dir']
        self.batch_size = args['batch_size']
        self.hidden_dim = args['hidden_dim']
        self.seq_length = args['seq_length']
        self.weights = args['weights']
        self.generate_length = args['generate_length']
        self.layer_num = args['layer_num']
        self.mode = args['mode']
        self.X = []
        self.y = []
        self.vocab_size = 0
        self.ix_to_char = None

    def load_data(self):
        self.X, self.y, self.vocab_size, self.ix_to_char = load_data(self.data_dir, self.seq_length)


def init_model(data: Data) -> Sequential:
    model = Sequential()
    model.add(LSTM(data.hidden_dim, input_shape=(None, data.vocab_size), return_sequences=True))
    for i in range(data.layer_num - 1):
        model.add(LSTM(data.hidden_dim, return_sequences=True))
        if i == 0:
            model.add(Dropout(0.3))
    model.add(TimeDistributed(Dense(data.vocab_size)))
    model.add(Activation('softmax'))
    model.compile(loss="categorical_crossentropy", optimizer="rmsprop")
    return model


def train_net(data: Data, model: Sequential):
    if not data.weights == '':
        model.load_weights(data.weights)
        nb_epoch = int(data.weights[data.weights.rfind('_') + 1:data.weights.find('.')])
    else:
        nb_epoch = 0

    while True:
        print('\n\nEpoch: {}\n'.format(nb_epoch))
        model.fit(data.X, data.y, batch_size=data.batch_size, verbose=1, nb_epoch=1)
        nb_epoch += 1
        generate_text(model, data.generate_length, data.vocab_size, data.ix_to_char)
        if nb_epoch % 10 == 0:
            model.save_weights('checkpoint_layer_{}_hidden_{}_epoch_{}.hdf5'.format(data.layer_num,
                                                                                    data.hidden_dim, nb_epoch))


def eval_net(data: Data, model: Sequential) -> str:
    model.load_weights(data.weights)
    return generate_text(model, data.generate_length, data.vocab_size, data.ix_to_char)


if __name__ == '__main__':
    data = Data(init_args())
    model = init_model(data)
    if data.mode == 'train':
        train_net(data, model)
    elif data.mode == 'test':
        text = eval_net(data, model)
        print(text)
    else:
        raise Exception("not find method run")

    print("DONE")
