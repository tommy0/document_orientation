import os


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

DATA_DIR = os.path.join(BASE_DIR, 'data')
IMAGE_DIR = os.path.join(DATA_DIR, 'image')
PDF_DIR = os.path.join(DATA_DIR, 'pdf')
META_DIR = os.path.join(DATA_DIR, "meta")
LOGS_DIR = os.path.join(BASE_DIR, 'logs')
MODELS_DIR = os.path.join(BASE_DIR, 'models')

MODEL_VERSION = "0.0.1"
MODEL_NAME = f"model_{MODEL_VERSION}"

LOG_FILE = os.path.join(LOGS_DIR, 'doc_rec.log')
ALL_INDEXES_FILE = os.path.join(MODELS_DIR, MODEL_NAME, "all.txt")
TEST_INDEXES_FILE = os.path.join(MODELS_DIR, MODEL_NAME, "test.txt")
TRAIN_INDEXES_FILE = os.path.join(MODELS_DIR, MODEL_NAME, "train.txt")

CLASS_MAP = {
    0: 'normal',
    1: 'revert',
}

IMAGE_FORMAT = 'png'
