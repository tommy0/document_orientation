from typing import List, Optional

from .font import Font
from .background import Background
from .effect import Effect
from .image import Image
from .text import Text
from .page import Page


class Document:
    font: Optional[Font]
    background: Optional[Background]
    title: Optional[Text]
    text: Optional[List[Text]]
    images: Optional[List[Image]]
    effects: Optional[List[Effect]]

    def __init__(self, font: Optional[Font] = None,
                 background: Optional[Background] = None,
                 title: Optional[Text] = None,
                 text: Optional[Text] = None,
                 images: Optional[List[Image]] = None,
                 effects: Optional[List[Effect]] = None):
        self.font = font
        self.background = background
        self.title = title
        self.text = text
        self.images = images
        self.effects = effects

    def get_count_page(self) -> int:
        pass

    def get_pdf(self):
        pass

    def save_pdf(self, path: str):
        pass

    def get_pages(self) -> List[Page]:
        pass
