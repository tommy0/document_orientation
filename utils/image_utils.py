import os
import cv2
import uuid
import ujson
import settings


def save_image(name: str, data: list):
    img_name = os.path.join(settings.DATA_DIR, name)
    retval = cv2.imwrite(img_name, data)
    return retval


def get_image_name(image_format: str = 'png', name: str = None) -> str:
    img_name = name if name else str(uuid.uuid4())
    return f"{img_name}.{image_format}"


def get_meta_name(meta_format: str = "json", name: str = None) -> str:
    return f"{name}.{meta_format}"


def save_meta(name: str, img_class: str, seed: int, *args, **kwargs):
    meta_name = get_meta_name(name)
    meta_path = os.path.join(settings.META_DIR, meta_name)
    meta_data = {
        "img": name,
        "path": os.path.join(settings.DATA_DIR, name),
        "class": img_class,
        "seed": seed,
        **kwargs
    }

    with open(meta_path, "w") as f:
        ujson.dump(meta_data, f)
