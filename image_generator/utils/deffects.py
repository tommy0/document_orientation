import ocrodeg
import ndi
import random
import settings
import os
import cv2


def scew(img):
    aniso = random.choice(50) / 10
    return ocrodeg.transform_image(img, aniso=aniso)


# phantom charcater
def phant(img):
    sigma = random.choice(50) / 12
    noise = ocrodeg.bounded_gaussian_noise(img.shape, sigma, 5.0)
    distorted = ocrodeg.distort_with_noise(img, noise)
    return distorted

# blur
def blur(img):
    s = random.choice(4)
    blurred = ndi.gaussian_filter(img, s)
    return blurred

def gray_degr(img):
    s = random.choice(10)
    blurred = ndi.gaussian_filter(img, s)
    thresholded = 1.0 * (blurred > 0.5)
    return thresholded

def shadow_bind(img):
    s = random.choice(100) / 1000
    return ndi.shadow_binde(img, s)

maps = {
    0: scew,
    1: phant,
    2: blur,
    3: gray_degr,
    4: shadow_bind
}

if __name__ == '__main__':
    ds_name = os.path.basename("")
    result_base_path = os.path.join(settings.IMAGE_DIR, ds_name)
    ds_path = os.path.join(result_base_path, "id6")
    for doc in os.listdir(ds_path):
        print(f"process {doc}")
        images_out_path = os.path.join(result_base_path, doc)
        img = cv2.imread(doc)
        for i in range(5):
            if random.choice(1):
               img = maps[i](img)

       # cv2.imwrite(images_out_path, img)
        cv2.imshow("lol", img)
        print(f"done {doc}")
