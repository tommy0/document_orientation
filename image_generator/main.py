import os
import settings


def init_dirs():
    if not os.path.isdir(settings.DATA_DIR):
        os.mkdir(settings.DATA_DIR)

    if not os.path.isdir(settings.IMAGE_DIR):
        os.mkdir(settings.IMAGE_DIR)

    if not os.path.isdir(settings.PDF_DIR):
        os.mkdir(settings.PDF_DIR)

    if not os.path.isdir(settings.META_DIR):
        os.mkdir(settings.META_DIR)

    if not os.path.isdir(settings.LOGS_DIR):
        os.mkdir(settings.LOGS_DIR)

    if not os.path.isdir(settings.MODELS_DIR):
        os.mkdir(settings.MODELS_DIR)


def main():
    init_dirs()


if __name__ == '__main__':
    main()
